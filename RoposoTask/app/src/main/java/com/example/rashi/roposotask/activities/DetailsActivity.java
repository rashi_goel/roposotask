package com.example.rashi.roposotask.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rashi.roposotask.util.CircleTransform;
import com.example.rashi.roposotask.R;
import com.example.rashi.roposotask.entity.CardItemInfo;
import com.squareup.picasso.Picasso;


/**
 * Created by Rashi on 16-03-2016.
 */
public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtDescription;
    private TextView txtTitle;
    private TextView txtAuthor;
    private TextView txtPostedOn;
    private ImageView imgView;
    private TextView txtLikesCount;
    private TextView txtCommentCount;
    private CardItemInfo item;
    private int selectedPosition;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if("is_following".equals(intent.getAction()))
            {
                item.setIs_following(true);
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.details_activity);

        initialize();

        item = (CardItemInfo) getIntent().getSerializableExtra("card_details");
        selectedPosition = getIntent().getIntExtra("position", 0);
        setDetails(item);

        IntentFilter filter = new IntentFilter("is_following");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);

    }

    private void initialize()
    {

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        txtTitle = (TextView) findViewById(R.id.txt_title);
        txtDescription = (TextView) findViewById(R.id.txt_desc);
        txtAuthor = (TextView) findViewById(R.id.txt_author);
        txtPostedOn = (TextView) findViewById(R.id.txt_posted_on);
        txtLikesCount = (TextView) findViewById(R.id.txt_likes_count);
        txtCommentCount = (TextView) findViewById(R.id.txt_comments_count);
        imgView = (ImageView) findViewById(R.id.imageView);

        findViewById(R.id.likes_layout).setVisibility(View.GONE);
        findViewById(R.id.comments_layout).setVisibility(View.GONE);
    }

    private void setDetails(CardItemInfo info)
    {
        if( item.getLikes_count()>0)
        {
            findViewById(R.id.likes_layout).setVisibility(View.VISIBLE);
            txtLikesCount.setText(String.valueOf(info.getLikes_count()));
        }
        if( item.getComment_count()>0)
        {
            findViewById(R.id.comments_layout).setVisibility(View.VISIBLE);
            txtCommentCount.setText(String.valueOf(info.getComment_count()));
        }

        if(info.is_following())
        {
            ((Button)findViewById(R.id.btn_follow)).setText("Following");
        }

        if(selectedPosition < 2)
        {
            Picasso.with(this)
                    .load(info.getImage())
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(imgView);

        }
        else if(selectedPosition >=2)
        {
            Picasso.with(this)
                    .load(info.getSi())
                    .transform(new CircleTransform())
                    .fit().centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(imgView);
        }

        if(info.getTitle() != null)
            txtTitle.setText(info.getTitle());
        else
            txtTitle.setText(getResources().getString(R.string.no_title_text));

        if(info.getDescription()!=null )
            txtDescription.setText(info.getDescription());
        else
            txtDescription.setText(getResources().getString(R.string.no_desc_text));

        if(info.getUsername()!=null)
            txtAuthor.setText(info.getUsername());
        else
            txtAuthor.setText(getResources().getString(R.string.no_author_text));

        if(info.getVerb()!=null)
            txtPostedOn.setText(info.getVerb());
        else
            txtPostedOn.setText(getResources().getString(R.string.no_verb_text));
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                super.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    protected void sendUpdatedData(int position) {
        Intent intent = new Intent("followed_item");
        intent.putExtra("followed_item_position", position);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    @Override
    public void onClick(View v) {
        Toast.makeText(this, "You have started following this item.", Toast.LENGTH_SHORT).show();
        ((Button)findViewById(R.id.btn_follow)).setText("Following");
        sendUpdatedData(selectedPosition);
    }
}
