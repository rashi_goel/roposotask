package com.example.rashi.roposotask.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.rashi.roposotask.util.CircleTransform;
import com.example.rashi.roposotask.R;
import com.example.rashi.roposotask.entity.CardItemInfo;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by Rashi on 15-03-2016.
 */
public class CardListAdapter extends RecyclerView.Adapter<CardListAdapter.CardItemHolder>{

    public List<CardItemInfo> itemList;
    Context context;
    ListItemClickListener listItemClickListener;
    private LayoutInflater inflater;

    public CardListAdapter(Context context, List<CardItemInfo> list) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        itemList = list;
    }

    //Interface For Button Click Event
    public interface ListItemClickListener {
        void btnClicked(View view, int position);
        void imgClicked(View view, int position);
    }

    public void setListItemClickListener(ListItemClickListener listItemClickListener) {
        this.listItemClickListener = listItemClickListener;
    }


    @Override
    public CardItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.card_item_layout, parent, false);
        CardItemHolder holder = new CardItemHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(CardItemHolder holder, int position) {

        CardItemInfo obj = itemList.get(position);

        if(obj.getTitle() == null)
        {
            holder.txtTitle.setText(context.getResources().getString(R.string.no_title_text));
        }
        else
        {
            holder.txtTitle.setText(obj.getTitle());
        }

        if(obj.getDescription() != null)
        {
            holder.txtDescription.setText(obj.getDescription());
        }
        else
        {
            holder.txtDescription.setText(context.getResources().getString(R.string.no_desc_text));
        }

        if(obj.getUsername() == null)
        {
            holder.txtAuthor.setText(context.getResources().getString(R.string.no_author_text));
        }
        else
        {
            holder.txtAuthor.setText(obj.getUsername());
        }

        if(position < 2)
        {
            Picasso.with(context)
                    .load(obj.getImage())
                    .fit()
                    .centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(holder.imgView);

        }
        else if(position >=2)
        {
            Picasso.with(context)
                    .load(obj.getSi())
                    .transform(new CircleTransform())
                    .fit().centerCrop()
                    .placeholder(R.drawable.placeholder_image)
                    .into(holder.imgView);
        }

        if(obj.is_following())
        {
            holder.btnFollow.setText("Following");
        }
        else
            holder.btnFollow.setText("Follow");
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class CardItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView txtTitle;
        public TextView txtDescription;
        public TextView txtAuthor;
        public ImageView imgView;
        public Button btnFollow;


        public CardItemHolder(View itemView) {
            super(itemView);

            txtTitle = (TextView) itemView.findViewById(R.id.txt_title);
            txtDescription = (TextView) itemView.findViewById(R.id.txt_description);
            txtAuthor = (TextView) itemView.findViewById(R.id.txt_author);
            imgView = (ImageView) itemView.findViewById(R.id.img);
            btnFollow = (Button) itemView.findViewById(R.id.btn_follow);
            btnFollow.setOnClickListener(this);
            imgView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

            switch (v.getId())
            {
                case R.id.btn_follow :  listItemClickListener.btnClicked(v, getPosition());
                    break;

                case R.id.img :   listItemClickListener.imgClicked(v, getPosition());
                    break;
            }
        }
    }

    public List<CardItemInfo> getList()
    {
        return itemList;
    }
}
