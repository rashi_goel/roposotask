package com.example.rashi.roposotask.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.rashi.roposotask.R;
import com.example.rashi.roposotask.adapters.CardListAdapter;
import com.example.rashi.roposotask.entity.CardItemInfo;
import com.google.gson.Gson;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements CardListAdapter.ListItemClickListener  {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private CardListAdapter adapter;
    private List<CardItemInfo> list;
    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if ("followed_item".equals(intent.getAction())) {
                int updatedPosition = intent.getIntExtra("followed_item_position",0);
                CardItemInfo cardItem = adapter.getList().get(updatedPosition);
                cardItem.setIs_following(true);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();

        try {
            Gson gson = new Gson();
            JSONArray jsonArray = new JSONArray(loadJSONFromAsset());
            List<CardItemInfo> dummyList = new ArrayList<>();
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                dummyList.add(i, gson.fromJson(jsonObject.toString(), CardItemInfo.class));
            }
            list.addAll(dummyList);
            adapter = new CardListAdapter(MainActivity.this, list);
            adapter.setListItemClickListener(MainActivity.this);
            recyclerView.setAdapter(adapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void initialize() {
        recyclerView = (RecyclerView) findViewById(R.id.card_items_recycler_view);
        // Setting the LayoutManager
        mLayoutManager = new LinearLayoutManager(MainActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        list = new ArrayList<>();

        IntentFilter filter = new IntentFilter("followed_item");
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver, filter);

    }

    private String loadJSONFromAsset() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = getAssets().open("data.json");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(is));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            bufferedReader.close();

            Log.d("X", "Response Ready:" + stringBuilder.toString());

            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMessageReceiver);
        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void btnClicked(View view, int position) {
        Toast.makeText(this, "You have started following this item.", Toast.LENGTH_SHORT).show();
        adapter.getList().get(position).setIs_following(true);
        adapter.notifyDataSetChanged();
        requestFollow();
    }

    @Override
    public void imgClicked(View view, int position) {
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("card_details", list.get(position));
        intent.putExtra("position", position);
        startActivity(intent);
    }

    protected void requestFollow() {
        Intent intent = new Intent("is_following");
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
